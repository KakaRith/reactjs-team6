import React from 'react'
import Slider from '../components/Slider'
import { AboutUs } from '../components/AboutUs'
import { Ourteams } from '../components/Ourteams'
import { Whatwedo } from '../components/Whatwedo'
import FeatureCause from '../components/FeatureCause'
import { Donates } from '../components/Donates'
import Testimonials from '../components/Testimonials'


export const Home = () => {
  return (<>
      <Slider/>
      <AboutUs/>
      <FeatureCause/>
      <Whatwedo/>
      <Donates/>
      <Ourteams/> 
      <Testimonials/>
    </>
  );
};
