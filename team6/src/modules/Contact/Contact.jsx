import React from 'react'
import { BackgroundImage } from '../components/BackgroundImage'
import { Contacts } from '../components/Contacts'


export const Contact = () => {
  return (
    <div>
      <BackgroundImage text={'Contact us'}/>
      <Contacts/>
    </div>
  )
}
