import React from 'react'
import { AboutUs } from '../components/AboutUs'
import { BackgroundImage } from '../components/BackgroundImage'
import { Whatwedo} from '../components/Whatwedo'
import { Ourteams } from '../components/Ourteams'
export const About = () => {
  return (
    <div>
      <BackgroundImage text={'About Us'}/>
      <AboutUs/>
      <Whatwedo/>
      <Ourteams/>
    </div>
  )
}
