import "./Button.css";

function Button({ text }) {
  return (
    <button className="custombtn rounded ">
      {text}
      <span className="rounded-circle ">
        <i className="fa fa-arrow-right "></i>
      </span>
    </button>
  );
}

export default Button;
