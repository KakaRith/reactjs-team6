import React from 'react'
import { Navbar } from '../components/Navbar'
import { Outlet } from 'react-router-dom'
import { Smallnav } from '../components/Smallnav'
import  MyFooter  from '../components/MyFooter'
import { Arrow } from '../components/Arrow'
import './Layout.css'


export const Layout = () => {
  return (
    <>
      <Smallnav />
      <div className="sticky-top">
        <Navbar />
     </div>
     <div className='LayoutOutlet'>
      <Outlet/>
      <MyFooter/>
     </div>
  
     <Arrow/>
     
    </>
  
  )
}
