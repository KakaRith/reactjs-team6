import React from 'react'
import { Page404s } from '../components/Page404s'
import { BackgroundImage } from '../components/BackgroundImage'


export const Page404 = () => {
  return (
    <div>
      <BackgroundImage text={'404 page'}/>
        <Page404s/>
    </div>
  )
}
