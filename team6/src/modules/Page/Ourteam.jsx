import React from "react";
import { BackgroundImage } from "../components/BackgroundImage";
import { Ourteams } from '../components/Ourteams'

export const Ourteam = () => {
  return (
    <div>
        <BackgroundImage text={'Our Team'}/>
        <Ourteams/>
    </div>
  );
};
