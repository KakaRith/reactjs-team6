import React from "react";
import { BackgroundImage } from "../components/BackgroundImage";
import { Donates } from "../components/Donates";
export const Donate = () => {
  return (
    <div>
      <BackgroundImage text={'Donate'}/>
      <Donates/>
    </div>
  );
};
