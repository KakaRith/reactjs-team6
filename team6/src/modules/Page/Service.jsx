import React from "react";
import { BackgroundImage } from "../components/BackgroundImage";
import { Ourteams } from "../components/Ourteams";

export const Service = () => {
  return (
    <div>
      <BackgroundImage text={'Service'}/>
      <Ourteams/>
    </div>
  );
};
