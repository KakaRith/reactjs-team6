import React from "react";
import { BackgroundImage } from "../components/BackgroundImage";
import MyFooter from "../components/MyFooter";
import Testimonials from "../components/Testimonials";

export const Testimonial = () => {
  return (
    <div>
      <BackgroundImage />
      <Testimonials />
    </div>
  );
};
