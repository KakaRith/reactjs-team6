import React, { useState } from 'react'


export const Arrow = () => {
    const [userScroll, setUserScroll] = useState(false)

  function userscroll() {
    if (window.scrollY >=300) {
      setUserScroll(true)
    } else {
      setUserScroll(false)
    }
  }
  function scrolltotop() {
    window.scrollTo(0,0)}

      
  window.addEventListener('scroll',userscroll)

    

  return (
    <div>
        <button onClick={scrolltotop} style={{position: 'fixed', bottom:'20px', right: '20px',zIndex:'99',transition : '0.8s'}} type="button" className={`btn btn-outline learn-more-btn ${!userScroll?'d-none': ''}`}><i className="fa-solid fa-arrow-up"></i></button>
        </div>
  )
}
