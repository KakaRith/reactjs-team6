import React, { useState } from 'react'
import '../../assets/css/Navbar.css'
import { Link } from 'react-router-dom'

export const Navbar = () => {
  const [isScroll, setIsScroll] = useState(false)

  function addColor() {
    if (window.scrollY >=20) {
      setIsScroll(true)
    } else {
      setIsScroll(false)
    }
  }

  window.addEventListener('scroll',addColor)
  const NavLinkStyle = {
    textDecoration: 'none',
    color:'white'
  }


  return (<>

      <div >
      <nav className={`container-fluid navBackground navbar navbar-expand-lg   ${isScroll? 'isScroll fixed-top' : ''} `} >
       <div className="container-fluid">
        <div className="navbar-brand" ><span className='nav-name'>Chari</span><span className='nav-name-2'>Team</span>
          </div>
         <button className="navbar-toggler navbar-light bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
           </button>
         <div  className="collapse navbar-collapse" id="navbarNav">
              <div className='w-100 d-flex justify-content-lg-end navlinkContainer'>
                
           <ul className="navbar-nav "  >
             <li className="nav-item nav-item-sm-margin d-flex align-items-center">
              <Link className="nav-link  " style={NavLinkStyle} aria-current="page" to="/"><p className='navlistp fw-bold mx-3'>Home</p></Link>
             </li>
             <li className="nav-item nav-item-sm-margin d-flex align-items-center">
              <Link className="nav-link "  style={NavLinkStyle} to="about"><p className='navlistp fw-bold mx-3'>About</p></Link>
             </li>
             <li className="nav-item nav-item-sm-margin d-flex align-items-center">
              <Link className="nav-link "  style={NavLinkStyle} to="/causes"><p className='navlistp fw-bold mx-3'>Causes</p></Link>
             </li>
             <li className='nav-item nav-item-sm-margin d-flex align-items-center '>     
              <div className="dropdown position-relative" style={NavLinkStyle} >
                  <p className=" dropdown-toggle navlistp fw-bold mx-3"  role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false"> Page </p>
                <div  className="dropdown-menu " aria-labelledby="dropdownMenuLink">
                  <Link className="dropdown-item" to='service'>Services</Link>
                  <Link className="dropdown-item" to='donate'>Donate</Link>
                  <Link className="dropdown-item" to='ourteam'>Our Team</Link>
                  <Link className="dropdown-item" to='testimonial'>Testimonial</Link>
                  <Link className="dropdown-item" to='page404'>404 page</Link>
                </div>
              </div>
             </li>
             
             <li className="nav-item nav-item-sm-margin d-flex align-items-center">
              <Link className="nav-link  " style={NavLinkStyle} to="/Contact"><p className='navlistp fw-bold mx-3'>Contact</p></Link>
             </li>
             <li className="nav-item nav-item-sm-margin d-flex align-items-center">
              <Link className="nav-link "  to="/"><p className='navlistp fw-bold  donatenow'>Donate Now</p></Link>
             </li>
           </ul> 
           </div>
       </div>   
    </div>
   </nav>
  </div>

    </>
  )
}

