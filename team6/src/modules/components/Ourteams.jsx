import React from 'react'
import '../../assets/css/Ourteam.css'
import team1 from '../../assets/img/team-1.jpg'
import team2 from '../../assets/img/team-2.jpg'
import team3 from '../../assets/img/team-3.jpg'
import team4 from '../../assets/img/team-4.jpg'
export const Ourteams = () => {

    const Ourteamdata = [{
        name: 'John',
        position: 'CEO',
        i: 1,
        img : team1
    },{
        name: 'Selena',
        position: 'Designer',
        i: 2,
        img : team2
    },{
        name: 'Alice',
        position: 'Web dev',
        i: 3,
        img : team3
    },{
        name: 'Joe',
        position: 'Designer',
        i: 4,
        img : team4
    }]
  return (
    <div >
        <div className='d-flex flex-column justify-content-center align-items-center mt-5 pt-5'>
            <p style={{background: '#fff0e6'}} className='orange py-1 px-3 rounded-pill'>Team Members</p>
          
          <h1 className='fw-bold text-center'>Let's Meet With Our <br /> Ordinary Soldiers</h1>
        </div>
        <div className='container'>      
        <div className='row '>

          {Ourteamdata.map((p) => {
              return (
                <div className='col-lg-3 col-sm-6 card_container my-2 overflow-hidden '>
                  <div className='' >
                    <div className='upperpart rounded '>
                    <img className='img-fluid  ' src={p.img} alt=""  />
                    </div>
                    <div className="outer-belowpart rounded">
                       <div className="belowpart " >
                        <h5 className='text-center pt-4 fw-bold'>{p.name}</h5>
                        <p className='text-center orange '>{p.position}</p> 
                        <div className='position-relative social_icon_container'>
                        <i className="fa-brands fa-facebook"></i>
                        <i className="fa-brands fa-twitter"></i>
                        <i className="fa-brands fa-instagram"></i>
                       </div>
                    </div>
                    </div>
                 </div>
                </div>  
              )
          })}
        </div>
          
    </div>
    </div>
  )
}
