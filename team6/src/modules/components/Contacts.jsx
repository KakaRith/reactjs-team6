import React from 'react'
import '../../assets/css/Contacts.css'
import Button from '../MicroComponent/CustomButton'

export const Contacts = () => {
  return (
    <div>
         <div className="container-xxl contact_page py-5">
        <div className="container">
            <div className="row g-5">
                <div className="col-lg-6 col-md-12">
                    <div className="contact_title d-inline-block rounded-pill py-1 px-3 mb-3">Contact Us</div>
                    <h1 className='contact_content mb-5'>If You Have Any Query, Please Contact Us</h1>
                    <p className=' contact_description text-secondary mb-4'>The contact form is currently inactive. Get a functional and working contact form with Ajax &amp; PHP in a few minutes. Just copy and paste the files, add a little code and you're done. 
                        <a className='download_link' href=""> Download Now</a>.
                    </p>
                    <form action="">
                        <div className="row">
                            <div className="col-md-6 mb-3">
                                <div class="form-floating">
                                    <input type="name" class="form-control" id="floatingInput" placeholder="Your Name"/>
                                    <label className='text-secondary fs-6' for="floatingInput">Your Name</label>
                                </div>
                            </div>
                            <div className="col-md-6 mb-3">
                                <div class="form-floating">
                                    <input type="email" class="form-control" id="floatingPassword" placeholder="Your Email"/>
                                    <label className='text-secondary fs-6' for="floatingPassword">Your Email</label>
                                </div>
                            </div>
                            <div className="col-12 mb-3">
                                <div class="form-floating">
                                    <input type="subject" class="form-control" id="floatingPassword" placeholder="Subject"/>
                                    <label className='text-secondary fs-6' for="floatingPassword">Subject</label>
                                </div>
                            </div>
                            <div className="col-12 mb-3">
                                <div class="form-floating">
                                    <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style={{height: "100px"}}></textarea>
                                    <label className='text-secondary fs-6' for="floatingTextarea2">Message</label>
                                </div>
                            </div>
                            <div className="col-12">
                                {/* <button type="button" class="btn btn-outline-warning px-4 py-2 g-2 fw-bold  ">Send Messsage
                                    <span className=' text-bg-light border-0 rounded-circle ms-3 p-1 '><i class="fa-solid fa-arrow-right fa-lg" style={{color: "#ff6f0f"}}></i></span>
                                </button> */}
                                <Button text={'Send Message'}/>  
                            </div>
                        </div>
                    </form>
                </div>
                <div className="col-lg-6 col-md-12">
                    <iframe className='position-relative w-100 h-100' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.8972224879853!2d104.88377230914793!3d11.559225188593745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310951473174f105%3A0x3ef0a2faf0cf330b!2sKiloIT!5e0!3m2!1sen!2skh!4v1705390582113!5m2!1sen!2skh" 
                    width="610" 
                    height="600" 
                    style={{ minHeight:"450px", border:"0"}}
                    allowfullscreen="" 
                    loading="lazy" 
                    referrerpolicy="no-referrer-when-downgrade">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
    </div>
  )
}
