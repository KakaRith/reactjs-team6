import React from 'react'
import Image from '../../assets/img/carousel-1.jpg'
import '../../assets/css/BackgroundImage.css'
export const BackgroundImage = ({text}) => {
  return (
    <div >
      <div  className='container-fluid  position-absolute '>
        <div style={{height:'400px'}} className='d-flex justify-content-center align-items-center'>
            <h1 className='text-white fw-bold '  style={{fontSize:'60px'}}>{text}</h1>
        </div>
      </div>
        <div className="container-fluid overflow-hidden backimgCon">
          <img className='backgroundImg' src={Image} alt="" />
         </div>
    
    </div>
  )
}
