import React from 'react'
import Image1 from '../../assets/img/about-1.jpg'
import Image2 from '../../assets/img/about-2.jpg'
import CustomButton from '../MicroComponent/CustomButton'
import '../../assets/css/AboutUs.css'

export const AboutUs = () => {
  return (
    <>
    <div className="container py-5">
      <div className="row">
        <div className="col-lg-6 position-relative px-3">
          <div className='about-img1Con'>
            <img className='img-fluid about-img p-4' src={Image1} alt="" />
          </div>
          <div>
            <img className='about-img2 mx-3' src={Image2} alt="" />
          </div>
        </div>
        <div className='col-lg-6 px-5'>
          <div className='d-flex flex-column justify-content-center align-items-start mt-5 pt-5'>
          <p style={{background: '#fff0e6'}} className='orange py-1 px-3 rounded-pill'>About us</p>
          
          <h1 className='fw-bold '>We Help People In Need <br /> Around The World</h1>

          <div className="container">
            <div className='text-Box p-3 my-5' style={{background:'#f8f8f9'}}>
              <p>Aliqu diam amet diam et eos. Clita erat ipsum et lorem sed stet lorem sit clita duo justo erat amet</p>
              <p className='orange'>
                Jhon Doe, Founder
              </p>
            </div>
            <div className=' my-5'>
              <p style={{color:'gray'}}>Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit. Aliqu diam amet diam et eos. Clita erat ipsum et lorem et sit, sed stet lorem sit clita duo justo magna dolore erat amet</p>
            </div>
            <div className='my-5'>
             <button className="btn-readmoress rounded p-2 me-3">
              <span className='textlearnmore px-2'>
                Learn More
              </span>
              <span className="rounded-circle">
                <span >
                  <i className="fa fa-arrow-right "></i>
                </span>
              </span>
             </button>
              <span>
                <CustomButton text={'Contact us'}/>
              </span>
            </div>
          </div>
        </div>

        </div>
      </div>
    </div>
    </>
  )
}