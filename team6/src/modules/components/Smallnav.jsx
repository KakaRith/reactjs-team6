import React from 'react'
import './Navbar'
export const Smallnav = () => {
  return (
    <div>   
    <div className='container-fluid  topnavbar_container d-none d-lg-block '>
      <div className='row py-1 '>
        <div className='col-lg-6'>
        <div className='m-1 d-flex'>
          <div className='ms-3'>
            <i className="fa-solid fa-location-dot"></i>
             <span className='ps-2'>123 Street, New York, USA</span>
          </div>
          <div className='ms-3'>
            <i className="fa-solid fa-envelope "></i>
             <span className='ps-2'>info@example.com</span>
          </div>
        </div>
      </div>
      <div className='col-lg-6'>
        <div className='d-flex justify-content-end m-1'>
          <div>
            <span>Follow us:</span>
            <span className='nav_icon_list'>
              <i className="fa-brands fa-facebook px-2"></i>
              <i className="fa-brands fa-twitter px-2"></i>
              <i className="fa-brands fa-linkedin px-2"></i>
              <i className="fa-brands fa-instagram px-2"></i>
            </span>
          </div>
        </div>
      </div>
      </div>
    </div>
      </div>
  )
}
