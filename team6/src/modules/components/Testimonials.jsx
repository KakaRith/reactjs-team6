import "../../assets/css/Testimonial.css";
import Image1 from "../../assets/img/testimonial-1.jpg";
import Image2 from "../../assets/img/testimonial-2.jpg";
import Image3 from "../../assets/img/testimonial-3.jpg";

import React, { Fragment } from "react";

const data = [Image1, Image2, Image3];

export default function Testimonials() {
  return (
    <Fragment>
      <header style={{ paddingTop: "100px" }}>
        <div className="small-text text-center" data-aos="fade-up">
          <p>Testimonial</p>
        </div>
        <div className="motto text-center" data-aos="fade-up">
          <h3
            style={{
              fontWeight: "bolder",
              fontSize: "2rem",
              fontWeight: "900",
              fontFamily: "Inter",
            }}
          >
            Trusted By Thousands of <br /> People And Nonprofits{" "}
          </h3>
        </div>
      </header>

      <div id="carouselExample" class="carousel slide">
        <div class="carousel-inner">
          <div class="carousel-item active non-active">
            <div className="card-header-testimonial d-flex row justify-content-center">
              <div
                className="p-2 rounded-circle"
                style={{ backgroundColor: "#ff6f0f", width: "fit-content" }}
              >
                <img className="rounded-circle" src={Image2} alt="" />
              </div>
              <div
                className="d-flex justify-content-center"
                style={{ marginTop: "1.5rem" }}
              >
                <div
                  className="p-4 rounded text-center w-75
                  "
                  style={{ backgroundColor: "#ff6f0f", color: "white" }}
                >
                  <p>
                    Clita clita tempor justo dolor ipsum amet kasd amet duo
                    justo duo duo labore sed sed. Magna ut diam sit et amet stet
                    eos sed clita erat magna elitr erat sit sit erat at rebum
                    justo sea clita.
                  </p>

                  <div className="person-info">
                    <p style={{ fontWeight: "800", marginBottom: "0px" }}>
                      Doner Name
                    </p>
                    <p style={{ margin: "0px" }}>Profession</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/**dwakdauwidwabi */}
          <div class="carousel-item non-active">
            <div className="card-header-testimonial d-flex row justify-content-center">
              <div
                className="p-2 rounded-circle"
                style={{ backgroundColor: "#ff6f0f", width: "fit-content" }}
              >
                <img className="rounded-circle" src={Image1} alt="" />
              </div>
              <div
                className="d-flex justify-content-center"
                style={{ marginTop: "1.5rem" }}
              >
                <div
                  className="p-4 rounded text-center w-75"
                  style={{ backgroundColor: "#ff6f0f", color: "white" }}
                >
                  <p>
                    Clita clita tempor justo dolor ipsum amet kasd amet duo
                    justo duo duo labore sed sed. Magna ut diam sit et amet stet
                    eos sed clita erat magna elitr erat sit sit erat at rebum
                    justo sea clita.
                  </p>

                  <div className="person-info">
                    <p style={{ fontWeight: "800", marginBottom: "0px" }}>
                      Doner Name
                    </p>
                    <p style={{ margin: "0px" }}>Profession</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/**dnwaidbwaidba */}
          <div class="carousel-item non-active">
            <div className="card-header-testimonial d-flex row justify-content-center">
              <div
                className="p-2 rounded-circle"
                style={{ backgroundColor: "#ff6f0f", width: "fit-content" }}
              >
                <img className="rounded-circle" src={Image3} alt="" />
              </div>
              <div
                className="d-flex justify-content-center"
                style={{ marginTop: "1.5rem" }}
              >
                <div
                  className="p-4 rounded text-center w-75"
                  style={{ backgroundColor: "#ff6f0f", color: "white" }}
                >
                  <p>
                    Clita clita tempor justo dolor ipsum amet kasd amet duo
                    justo duo duo labore sed sed. Magna ut diam sit et amet stet
                    eos sed clita erat magna elitr erat sit sit erat at rebum
                    justo sea clita.
                  </p>

                  <div className="person-info">
                    <p style={{ fontWeight: "800", marginBottom: "0px" }}>
                      Doner Name
                    </p>
                    <p style={{ margin: "0px" }}>Profession</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <button
            class="carousel-control-prev set-arrow-right set-arrow-left"
            type="button"
            data-bs-target="#carouselExample"
            data-bs-slide="prev"
            style={{ color: "#ff6f0f", fontSize: "30px" }}
          >
            <span class="fa fa-arrow-left" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button
            class="carousel-control-next set-arrow-right"
            type="button"
            data-bs-target="#carouselExample"
            data-bs-slide="next"
            style={{ color: "#ff6f0f", fontSize: "30px" }}
          >
            <span class="fa fa-arrow-right" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
      </div>
    </Fragment>
  );
}
