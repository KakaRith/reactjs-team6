import React from 'react';
import '../../assets/css/Donate.css';
import Image2 from '../../assets/img/carousel-2.jpg';

export const Donates = () => {
    return (
        <div className='p-2 container-fluid wholedonate overflow-hidden position-relative'>
            <div className=' mx-auto grid  d-flex justify-content-center py-5 px-5 donate-container position-relative'>
             <div className='text  mx-auto my-4 '>
             <div className='d-flex justify-content-start align-items-center'>
             <p style={{background: '#fff0e6'}} className='orange py-1 px-3 rounded-pill'>Donate Now</p>
             </div>
                <h1 className='text-white fw-bold'>Thanks For The Results Achieved With You</h1>
                <p className='text-white'>Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit. Aliqu diam amet diam <br />
                et eos. Clita erat ipsum et lorem et sit, sed stet lorem sit clita duo justo magna <br/>
                dolore erat amet</p>
              </div>
             <div className='form bg-white align-item flex flex-column justify-center form-containers'>
                <div className='mb-3'>
                <input className='bg-light w-100 p-3'type='text' name='name' id='' placeholder='Your Name' style={{border: 'none'}}></input>
                </div>
                <div className='mb-3'>
                <input className='bg-light w-100 p-3'type='text' name='name' id='' placeholder='Your Email' style={{backgroundColor:'#afafb6', border: 'none'}}></input>
                </div>
                <div className='mb-3'>
                <button className='w-50 p-2 orange' style={{borderColor: '#FF6F0F'}}>$10</button>
                <button className='w-50 p-2 orange' style={{border: 'none'}}>$20</button>
                <button className='w-50 p-2 orange' style={{border: 'none'}}>$30</button>
                </div>
                <div>
                <button className='btn btn-outline w-50 p-2 text-white' style={{background: '#FF6F0F', border: 'none'}}>Donate Now</button>
                </div>
            </div>
            
            </div>
            <img className=' donate-background-img ' src={Image2} alt="" />
        </div>
    );
};