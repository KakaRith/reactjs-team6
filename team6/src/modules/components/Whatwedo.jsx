import React from 'react'
import '../../assets/css/Whatwedo.css'
import { Whatwedodata } from '../../assets/data/Whatwedodata'
import CustomButton from '../MicroComponent/CustomButton'

export const Whatwedo = () => {
  return (
    <>
    <div className='d-flex flex-column justify-content-center align-items-center mt-5 pt-5'>
            <p style={{background: '#fff0e6'}} className='orange py-1 px-3 rounded-pill'>What We Do</p>
          
          <h1 className='fw-bold text-center'>Learn More What We Do <br /> And Get Involved</h1>
        </div>
    <div className="container">
        <div className='row '>
             {Whatwedodata.map((item) =>{
              return (<div className="col-lg-4 ">
                <div className=' d-flex justify-content-center'>
                  <div className='wwd-card-container my-5 p-5'>
                    <div className='d-flex justify-content-center'>
                      <img src={item.icon} alt="" />
                    </div>
                    <h4 className='text-center fw-bold my-3' >{item.title}</h4>
                    <p className='text-center fw-normal wwd-des'>{item.description}</p>
                   <div className='d-flex justify-content-center mt-3'>
                     <CustomButton text={'Learn More'}/>
                   </div>
                  </div>
                </div>
              </div>)
             })}
            


       
       </div>
    </div>
    </>
  )
}
