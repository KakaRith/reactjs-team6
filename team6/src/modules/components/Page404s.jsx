import React from 'react'
import Button from '../MicroComponent/CustomButton'
export const Page404s = () => {
  return (
    <div>
        <div className="container my-5">
          <div className='d-flex justify-content-center pt-3'>
            <i className="fa-solid fa-triangle-exclamation" style={{fontSize: '70px', color: '#ff6f0f'}}></i>
          </div>
            <div>
              <h1 style={{fontSize : '80px'}} className="fw-bold text-center pt-2">404</h1>
              <h1 className='fw-bold text-center'>Page not found</h1>
              <p className='text-center pt-2 ' style={{color: 'gray'}}>We’re sorry, the page you have looked for does not exist in our website! <br /> Maybe go to our home page or try to use a search?</p>
             <div className='d-flex justify-content-center py-2'>
               <Button text={'Go Back to Home'}/>
             </div>
            </div>
        </div>

    </div>
  )
}
