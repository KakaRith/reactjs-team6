import { Fragment} from "react";
import "../../assets/css/cause.css";
import course1 from "../../assets/img/courses-1.jpg";
import course2 from "../../assets/img/courses-2.jpg";
import course3 from "../../assets/img/courses-3.jpg";
// ..

const data = [
  {
    key: 1,
    category: "Education",
    title: "Education For African Children",
    description:
      "We neeed to set up more school across Africa in order to improve education System",
    moneyRaise: "9523",
    image: course1,
  },
  {
    key: 2,
    category: "Pure Water",
    title: "Ensure Pure Drinking Water",
    description:
      "We neeed to set up more school across Africa in order to improve education System",
    moneyRaise: "9523",
    image: course2,
  },
  {
    key: 3,
    category: "Healthy Life",
    title: "Ensure Medical Treatment",
    description:
      "We neeed to set up more school across Africa in order to improve education System",
    moneyRaise: "9523",
    image: course3,
  },
];

//*  target goals donation

const Donation = ({ percent }) => {

  return (
    <Fragment>
      <main className="donation-goal bg-white w-75">
        <div className="d-flex justify-content-between">
          <div className="amount-text">$10000 Goal</div>
          <div className="amount-text">$9245 Raise</div>
        </div>
        <div className="goal-bar">
          <div className="percent-container">
            <p className="percent" >
              {percent}%
            </p>
          </div>
        </div>
      </main>
    </Fragment>
  );
};

const ReadMore = ({ image }) => {
  return (
    <div className="container-readmore">
      <img className="img-fluid" src={image} alt="" />
      <div className="bg-btn w-100">
        <button className="btn-readmore rounded">
          Read More{" "}
          <span className="rounded-circle">
            <i className="fa fa-arrow-right"></i>
          </span>
        </button>
      </div>
    </div>
  );
};

export default function FeatureCause() {
  return (
    <Fragment>
      <section className="w-75 pt-0 mt-5">
        <div className="small-text text-center" >
          <p>Feature Causes</p>
        </div>
        <div className="motto text-center" >
          <h4>
            Every Child Deserves The <br /> Opportunity To{" "}
          </h4>
        </div>

        <main className="d-flex flex-column flex-sm-row">
          {data.map(({ key, category, title, description, image }) => {
            return (
              <div className="cause-card rounded text-center col">
                <div className="text-container">
                  <p className="cat-text">{category}</p>
                  <p className="title-text">{title}</p>
                  <p className="description-text">{description}</p>
                </div>

                <Donation percent={90} />
                <ReadMore image={image} />
              </div>
            );
          })}
        </main>
      </section>
    </Fragment>
  );
}
