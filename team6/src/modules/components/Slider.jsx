import Image1 from '../../assets/img/carousel-1.jpg';
import Image2 from '../../assets/img/carousel-2.jpg';
import '../../assets/css/Slider.css'
function Slider() {
  return (
    <>
<div style={{background:'#01171d'}} id="carouselExampleCaptions" className="carousel slide" data-bs-ride="carousel">
  <div className="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active d-none" aria-current="true" aria-label="Slide 1"></button>
    <button className='d-none' type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
  </div>
  <div className="carousel-inner">
    <div className="carousel-item active">
      <img style={{opacity:'0.2'}} src={Image1} className="d-block w-100" alt="..."/>
      <div className="carousel-caption d-none d-md-block">
        <h1 className='fw-bold'>Let's Change The World <br /> With Humanity</h1>
        <p>Aliqu diam amet diam et eos. Clita erat ipsum et lorem sed stet <br /> lorem sit clita duo justo erat amet</p>
        <button type="button" className="btn btn-outline learn-more-btn">Learn more</button>
      </div>
    </div>
    <div className="carousel-item">
      <img style={{opacity:'0.2'}} src={Image2} className="d-block w-100" alt="..."/>
      <div className="carousel-caption d-none d-md-block">
       <h1 className='fw-bold'>Let's Save More Lifes <br /> With Our Helping Hand</h1>
        <p>Some representative placeholder content for the second slide.</p>
         <button type="button" className="btn btn-outline learn-more-btn">Learn more</button>
      </div>
    </div>
  </div>
  <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Previous</span>
  </button>
  <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Next</span>
  </button>
   </div>
    </>
  );
}

export default Slider;
