import React from "react";
import { BackgroundImage } from "../components/BackgroundImage";
import FeatureCause from "../components/FeatureCause";

export const Causes = () => {
  return <div>
     <BackgroundImage text={'Causes'}/>
     <FeatureCause/>
  </div>;
};
