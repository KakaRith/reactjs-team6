import './App.css';
import { Home } from './modules/Home/Home';
import { Routes , Route } from 'react-router-dom';
import { Layout } from './modules/Layout/Layout';
import { About } from './modules/About/About';
import { Contact } from './modules/Contact/Contact'
import { Causes } from './modules/Causes/Causes'
import { Service } from './modules/Page/Service'
import { Donate } from './modules/Page/Donate';
import { Testimonial } from './modules/Page/Testimonial';
import { Page404 } from './modules/Page/Page404';
import { Ourteam } from './modules/Page/Ourteam';

function App() {
  return (<>
    <Routes>                                                           
      <Route path="/" element={<Layout/>}>
        <Route index element={<Home />} />
        <Route path='about' element={<About />} />
        <Route path='causes' element={<Causes/>}/>
        <Route path='donate' element={<Donate/>} />
        <Route path='ourteam' element={<Ourteam/>} />
        <Route path='page404' element={<Page404/>} />
        <Route path='service' element={<Service/>} />
        <Route path='testimonial' element={<Testimonial/>} />
        <Route path='contact' element={<Contact />} />
      </Route>
   </Routes>
  </>
  );
}

export default App;
