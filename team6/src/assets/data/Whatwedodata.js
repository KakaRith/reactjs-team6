import Icon1 from '../img/icon-1.png'
import Icon2 from '../img/icon-2.png'
import Icon3 from '../img/icon-3.png'

const Whatwedodata = [
  { id: 1,
    icon: `${Icon1}`,
    title: 'Child Education',
    description: 'Tempor ut dolore lorem kasd vero ipsum sit eirmod sit. Ipsum diam justo sed vero dolor duo.'
},
  { id: 2,
    icon: `${Icon2}`,
    title: 'Medical Treatment',
    description: 'Tempor ut dolore lorem kasd vero ipsum sit eirmod sit. Ipsum diam justo sed vero dolor duo.'

},
  { id: 3,
    icon: `${Icon3}`,
    title: 'Pure Drinking Water',
    description: 'Tempor ut dolore lorem kasd vero ipsum sit eirmod sit. Ipsum diam justo sed vero dolor duo.'
},
];

export { Whatwedodata };